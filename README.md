trisect_web_video_server
================

This code is forked from [web_video_server](https://github.com/RobotWebTools/web_video_server) and modified to fit the needs of the [Trisect](https://rsa-perception-sensor.gitlab.io/trisect-docs/) project.  It retains the [BSD License](LICENSE) of `web_video_server`.  Thanks [RobotWebTools](http://robotwebtools.org).

Modifications relative to `web_video_server`:

 * replace `height` and `width` arguments with a single `scale` floating point value.
 * Added hand-coded two- and three-stream "stereo" and "triplet" pages.



----
# Original web_video_server README follows.

#### HTTP Streaming of ROS Image Topics in Multiple Formats
This node combines the capabilities of [ros_web_video](https://github.com/RobotWebTools/ros_web_video) and [mjpeg_server](https://github.com/RobotWebTools/mjpeg_server) into a single node.

For full documentation, see [the ROS wiki](http://ros.org/wiki/web_video_server).

[Doxygen](http://docs.ros.org/indigo/api/web_video_server/html/) files can be found on the ROS wiki.
[mjpegcanvasjs](https://github.com/rctoris/mjpegcanvasjs) can be used to display a MJPEG stream from the ROS web_video_server

This project is released as part of the [Robot Web Tools](http://robotwebtools.org/) effort.

### License
web_video_server is released with a BSD license. For full terms and conditions, see the [LICENSE](LICENSE) file.

### Authors
See the [AUTHORS](AUTHORS.md) file for a full list of contributors.
