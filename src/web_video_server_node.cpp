#include <nodelet/loader.h>
#include <ros/ros.h>

int main(int argc, char **argv) {
  ros::init(argc, argv, "web_video_server");

  nodelet::Loader manager(true);
  nodelet::M_string remappings;
  nodelet::V_string my_argv;

  manager.load(ros::this_node::getName(), "trisect_web_video_server/nodelet",
               remappings, my_argv);

  ros::spin();

  return 0;
}
